package com.reactive.reactivejavastart.observable;

import io.reactivex.Observable;
import org.junit.jupiter.api.Test;

/**
 * The type Observable test.
 */
public class ObservableTest {

    /**
     * Observable test.
     * Наблюдатель генерирует события, а подписчик (subscriber) ихх обрабатывает.
     * Первый реактивный метод ).
     */
    @Test
    public void observableTest() {
        //noinspection ResultOfMethodCallIgnored
        Observable.create(
                sub -> {
                    sub.onNext("Hello? reactive world!");
                    sub.onComplete();
                }
        ).subscribe(System.out::println, System.out::println, () -> System.out.println("Done"));
    }

}
