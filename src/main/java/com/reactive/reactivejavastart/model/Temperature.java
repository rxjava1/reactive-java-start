package com.reactive.reactivejavastart.model;

import lombok.Data;

@Data
public final class Temperature {
    private final double value;
}
