package com.reactive.reactivejavastart;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableAsync;

@SpringBootApplication
@EnableAsync
public class ReactiveJavaStartApplication {

    public static void main(String[] args) {
        SpringApplication.run(ReactiveJavaStartApplication.class, args);
    }

}
