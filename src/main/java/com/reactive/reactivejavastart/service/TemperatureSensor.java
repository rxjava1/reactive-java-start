package com.reactive.reactivejavastart.service;

import com.reactive.reactivejavastart.model.Temperature;
import lombok.RequiredArgsConstructor;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.util.Random;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

@Component
@RequiredArgsConstructor
public class TemperatureSensor {
    private final ApplicationEventPublisher applicationEventPublisher;
    private final Random rnd = new Random();
    private final ScheduledExecutorService scheduledExecutorService = Executors.newSingleThreadScheduledExecutor();

    @PostConstruct
    public void startProcessor() {
        this.scheduledExecutorService.schedule(this::probe, 1, TimeUnit.SECONDS);
    }

    public void probe() {
        double temperature = 16 + rnd.nextGaussian() * 10;
        applicationEventPublisher.publishEvent(new Temperature(temperature));
        scheduledExecutorService.schedule(this::probe, rnd.nextInt(5000), TimeUnit.MILLISECONDS);
    }
}
